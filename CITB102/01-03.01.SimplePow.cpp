#include <iostream>
#include <cmath>
using namespace std;

int main()
{
    double num;

    cout << "Enter a number: ";
    cin >> num;

    for(int i = 2; i < 6; i++)
    {
        cout << num << "^" << i << "=" << powf(num, i); // using powf for floating point numbers
        //cout << num << "^" << i << "=" << pow(num, i) << ", "; // use pow for integers/whole numbers
        if (i != 5)
        {
            cout << ", ";
        }
    }

    return 0;
}
