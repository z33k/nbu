#include <cmath>
#include <iostream>

using namespace std;

int main()
{
    double a, b, c, p, triangleArea;

    cout << "Enter sides: ";
    cin >> a >> b >> c;

    if ( (a > b + c || ( b > a + c) || (c > a + b)))
    {
        cout << "Such triangle can't exists!";
    }
    else
    {
        p = (a + b + c) / 2;
        triangleArea = sqrt(p * (p - a)*(p - b)*(p - c));

        cout << triangleArea << endl;
    }

    return 0;
}
