#include <cmath>
#include <iostream>

using namespace std;

int main()
{
    double a, b, c;
    double D, x1, x2;

    cout << "Enter a, b and c: ";
    cin >> a >> b >> c;

    D = (b * b) - (4 * a * c);

    if (D < 0)
    {
        cout << "No solutions!";
    }
    else
    {
        x1 = (-b + sqrt(D)) / 2 * a;
        x2 = (-b - sqrt(D)) / 2 * a;

        cout << x1 << endl;
        cout << x2 << endl;
    }

    return 0;
}
