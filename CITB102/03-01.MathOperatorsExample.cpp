#include <iostream>
using namespace std;

int main()
{
	float a, b;

	cout << "Enter two numbers: ";
	cin >> a >> b;
	cout << endl;

	cout << a + b << endl;
	cout << a - b << endl;
	cout << a * b << endl;
	cout << a / b << endl;

	return 0;
}