// Go to Settings => Compiler and check "Have g++ follow the C+11 standart..."
#include <iostream>
#include <string>
#include <unordered_map>

using namespace std;

int main()
{
    unordered_map <char, int> grades;
    grades.emplace('A', 4);
    grades.emplace('B', 3);
    grades.emplace('C', 2);
    grades.emplace('D', 1);
    grades.emplace('F', 0);

    string input;
    char letter, symbol;
    double score = 0, bonus = 0.3;

    cout << "Enter a grade: ";
    cin >> input;

    letter = input[0];
    symbol = input[1];

    auto srch = grades.find (letter);
    if (srch == grades.end())
    {
        cout << "Bad input! Letters must be A, B, C, D, or F";
        return 0;
    }
    else
    {
        score = grades[letter];

        if (input.size() <= 2)
        {
            if (symbol == '+')
            {
                if (srch->first != 'A')
                {
                    score += bonus;
                }
            }
            else if (symbol == '-')
            {
                if (srch->first != 'F')
                {
                    score -= bonus;
                }
            }
        }
        else
        {
            cout << "Bad input! Only + and - are allowed after the letters!";
            return 0;
        }

        cout << "Your average grade is " << score;
    }

    return 0;
}
