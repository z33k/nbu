#include <iostream>
#include <unordered_map>
#include <sstream>
using namespace std;

int main()
{
    double firstNum, secondNum, result;
    string firstNumAsStr, secondNumAsStr;
    stringstream convert;
    char operatorChr;

    //A very short list of numbers
    //If the number is not in the hash table,
    //it'll be printed the way it's entered (as number)
    unordered_map <int, string> numbers;
    numbers.emplace(0, "zero");
    numbers.emplace(1, "one");
    numbers.emplace(2, "two");
    numbers.emplace(3, "three");
    numbers.emplace(4, "four");
    numbers.emplace(5, "five");
    numbers.emplace(6, "six");
    numbers.emplace(7, "seven");
    numbers.emplace(8, "eight");
    numbers.emplace(9, "nine");
    numbers.emplace(10, "ten");

    cout << "Enter operation(+/-/x/:) ";
    cin >> operatorChr;

    cout << "Enter operand 1: ";
    cin >> firstNum;

    cout << "Enter operand 2: ";
    cin >> secondNum;

    auto srch = numbers.find (firstNum);
    if (srch != numbers.end())
    {
        firstNumAsStr = srch->second;
    }
    else
    {
        convert << firstNum;
        firstNumAsStr = convert.str();
        convert.str("");
    }

    srch = numbers.find (secondNum);
    if (srch != numbers.end())
    {
        secondNumAsStr = srch->second;
    }
    else
    {
        convert << secondNum;
        secondNumAsStr = convert.str();
    }

    switch (operatorChr)
    {
    case '+':
        result =  firstNum + secondNum;
        cout << firstNumAsStr << " plus " << secondNumAsStr << " is " << result << endl;
        break;
    case '-':
        result =  firstNum - secondNum;
        cout << firstNumAsStr << " minus " << secondNumAsStr << " is " << result << endl;
        break;
    case 'x':
        result =  firstNum * secondNum;
        cout << firstNumAsStr << " multiplied by " << secondNumAsStr << " is " << result << endl;
        break;
    case ':':
        if (secondNum != 0)
        {
            result =  firstNum / secondNum;
            cout << firstNumAsStr << " divided by " << secondNumAsStr << " is " << result << endl;
        }
        else
        {
            cout << "Division by zero!" << endl;
        }
        break;
    default :
        //throw "Allowed operators are : +/-/x/:"; // throwing uncaught exception
        cout << "Bad input! Allowed operators are : +/-/x/:";
        return 0;
    }

    return 0;
}
