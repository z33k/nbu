// Go to Settings => Compiler and check "Have g++ follow the C+11 standart..."

#include <unordered_map>
#include <iostream>
#include <string>

using namespace std;

int main()
{
    string card, type, suit;

    cout << "Enter card: ";
    cin >> card;

    unordered_map <string, string> cards;
    cards.emplace("2", "Two");
    cards.emplace("3", "Three");
    cards.emplace("4", "Four");
    cards.emplace("5", "Five");
    cards.emplace("6", "Six");
    cards.emplace("7", "Seven");
    cards.emplace("8", "Eight");
    cards.emplace("9", "Nine");
    cards.emplace("10", "Ten");
    cards.emplace("J", "Jack");
    cards.emplace("Q", "Queen");
    cards.emplace("K", "King");
    cards.emplace("A", "Ace");

    unordered_map <string, string> suits;
    suits.emplace("D", "Diamonds");
    suits.emplace("H", "Hearts");
    suits.emplace("S", "Spades");
    suits.emplace("C", "Clubs");

    if (card.length() == 2)
    {
        type = card[0];
        suit = card[1];
    }
    else if (card.length() == 3)
    {
        type = card.substr(0, 2);
        suit = card[2];
    }
    else
    {
        cout << "Bad input!";
    }

    cout << cards[type] << " of " << suits[suit] << endl;
    return 0;
}
