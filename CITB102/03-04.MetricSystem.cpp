#include <iostream>
using namespace std;

int main()
{
    float meters;

    const int mToKm = 1000;
    const int mToDm = 10;
    const int mToCm = 10000;

    cout << "Enter a number: ";
    cin >> meters;

    cout << "KM: " << meters / mToKm << endl;
    cout << "DM: " << meters * mToDm << endl;
    cout << "CM: " << meters * mToCm << endl;

    return 0;
}