#include <iostream>
using namespace std;

int main()
{
	float goodsPrice, goodsQuantity, change;

	cout << "Enter the price of your goods(lv): ";
	cin >> goodsPrice;

	cout << "Enter the quantity of your goods(lv): ";
	cin >> goodsQuantity;

	change = goodsQuantity - goodsPrice;
	cout << "Your change is " << change << " (" << change * 100 << ")" << endl;

	return 0;
}