#include <iostream>
#include <string>

using namespace std;

void horLine()
{
    for (int i = 0; i < 19; i++)
    {
        cout << "-";
    }
    cout << endl;
}

void verLine()
{
    string info = "- Made by Victor!";
    for (int i = 0; i < 9; i++)
    {
        if (i == 4)
        {
            cout << info;
            for (int y = 0; y <= 19 - info.length(); y++)
            {
                cout << " ";
            }
            cout << "-" << endl;
            continue;
        }

        cout << "-";
        for (int x = 0; x < 19; x++)
        {
            cout << " ";
        }
        cout << "-";
        cout << endl;
    }
}

int main()
{
    string firstName, lastName, userAddress;
    string hello = "Hello, ";

    cout << "Enter you address: ";
    getline(cin, userAddress);

    cout << "Enter your first and last name: ";
    cin >> firstName >> lastName;

    cout << hello << firstName << " " << lastName << "! :<\n";
    cout << "You're from: " << userAddress << endl << endl;

    horLine();
    verLine();
    horLine();

    return 0;
}