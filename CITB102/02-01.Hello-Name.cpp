#include <iostream>
#include <string>
using namespace std;

int main()
{
	string firstName, lastName;

	cout << "Enter your first name: ";
	cin >> firstName;

	cout << "Enter your last name: ";
	cin >> lastName;

	cout << "Hello, " << firstName << " " << lastName << "!\n";
	return 0;
}
