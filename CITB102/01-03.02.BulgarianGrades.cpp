#include <iostream>
using namespace std;

int main()
{
    double firstGrade, secondGrade, averageGrade;

    cout << "Enter grade 1: " << endl;
    cin >> firstGrade;

    if (firstGrade < 2 || firstGrade > 6)
    {
        cout << "This grade is not valid in Bulgaria." << endl;
        return 0;
    }

    cout << "Enter grade 2: " << endl;
    cin >> secondGrade;

    if (secondGrade < 2 || secondGrade > 6)
    {
        cout << "This grade is not valid in Bulgaria." << endl;
        return 0;
    }

    averageGrade = (firstGrade + secondGrade) / 2;
    cout << "Your average grade is " << averageGrade << endl;

    return 0;
}
