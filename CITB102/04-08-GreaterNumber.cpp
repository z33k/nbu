#include <iostream>

using namespace std;

int main()
{
    float numbers[3], greaterNum = 0;

    for (int i = 0; i < sizeof(numbers) / 4; i++)
    {
        cout << "Enter number " << i + 1 << ": ";
        cin >> numbers[i];
        cout << endl;
    }

    for (int y = 0; y < sizeof(numbers); y++)
    {
        if (greaterNum < numbers[y])
        {
            greaterNum = numbers[y];
        }
    }

    cout << "Greatest numbers is: " << greaterNum;
    return 0;
}
