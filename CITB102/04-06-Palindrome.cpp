#include <string>
#include <iostream>

using namespace std;

int main()
{
    string str;
    bool isPal = true;

    cout << "Enter your string: ";
    cin >> str;

    int len = str.length();

    for (int i = 0; i < len / 2; i++)
    {
        if (str[i] != str[len - i - 1])
        {
            isPal = false;
            break;
        }
    }

    if (isPal == false)
    {
        cout << "It is not Palindrome!" << endl;
        cout << "LTR: " << str << endl;
        cout << "LTR: ";
        for (int y = len - 1; y >= 0; y--)
        {
            cout << str[y];
        }
        cout << endl;
    }
    else
    {
        cout << "It is Palindrome!";
    }
    return 0;
}
