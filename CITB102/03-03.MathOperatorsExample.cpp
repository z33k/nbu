#include <iostream>
#include <cmath>

using namespace std;

int main()
{
	float a, b;

	cout << "Enter two numbers: ";
	cin >> a >> b;
    cout << endl;

	cout << "a + b = " << a + b << endl;
	cout << "a - b = " << a - b << endl;
	cout << "a * b = " << a * b << endl;

	if (b != 0)
	{
        cout << "a / b = " << a / b << endl;
	}
	else
    {
        cout << "Divison by zero!";
    }

	return 0;
}
