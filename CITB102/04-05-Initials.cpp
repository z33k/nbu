#include <string>
#include <iostream>

using namespace std;

int main()
{
    string firstName, middleName, thirdName;
    char gender;

    cout << "Enter your full name: ";
    cin >> firstName >> middleName >> thirdName;

    cout << "Enter F for female and M for male: ";
    cin >> gender;

    if (gender == 'M' || gender == 'm')
    {
        cout << "Mr. " << thirdName;
    }
    else
    {
        cout << "Mrs. " << thirdName;
    }

    cout << ", your initials are " << firstName[0] << middleName[0] << thirdName[0];
    return 0;
}
