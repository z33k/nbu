#include <iostream>
#include <string>
using namespace std;

int main()
{
    char driveLetter[2], folderPath[256], fileName[256];

    cout << "Enter drive letter: ";
    cin.getline (driveLetter, 2);

    cout << "Enter path: ";
    cin.getline (folderPath, 256);

    cout << "Enter file name: ";
    cin.getline (fileName, 256);

    cout << driveLetter << "\\" << folderPath << "\\" << fileName << endl;

    return 0;
}
