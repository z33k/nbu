#include <iostream>
#include <string>
using namespace std;

int main()
{
    double oldValue, newValue;
    double degree = 0.0174532925;
    double radian = 57.2957795;
    char type;

    cout << "Enter a value : ";
    cin >> oldValue;

    cout << "Enter D for degrees and R for radiants: ";
    cin >> type;

    if (type == 'R' || type == 'r')
    {
        newValue = oldValue * radian;
    }
    else if (type == 'D' || type == 'd')
    {
        newValue = oldValue * degree;
    }
    else
    {
        cout << "Bad input!" << endl;
        return 0;
    }

    cout << newValue << endl;
    return 0;
}
