#include <iostream>
using namespace std;

int main()
{
	float tankSize, fuelEfficiency, pricePerLitter;

	cout << "Enter a tank size: ";
	cin >> tankSize;

	cout << "Enter fuel efficiency (km per liter): ";
	cin >> fuelEfficiency;

	cout << "Enter price per liter: ";
	cin >> pricePerLitter;

	cout << "We can travel " << tankSize * fuelEfficiency << " km." << endl;
	cout << "For every 100 km it will cost " << fuelEfficiency * pricePerLitter << " lv." << endl;

	return 0;
}
