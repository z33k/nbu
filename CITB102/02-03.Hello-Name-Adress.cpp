#include <iostream>
#include <string>

using namespace std;

void horLine(int maxLen)
{
    for (int i = 0; i <= maxLen; i++)
    {
        cout << "-";
    }
    cout << endl;
}


int main()
{
    string firstName, lastName, userAddress;

    cout << "Enter you address: ";
    getline(cin, userAddress);

    cout << "Enter your first and last name: ";
    cin >> firstName >> lastName;

    int maxLen = userAddress.length();

    horLine(maxLen);
    cout << "> " << firstName << endl;
    cout << "> " << lastName << endl;
    cout << "> " << userAddress << endl;
    horLine(maxLen);

    return 0;
}
