#include <iostream>
#include <cmath>

using namespace std;

int main()
{
    int num;

    cout << "Enter a number: " << endl;
    cin >> num;

    for(int i = 2; i < 6; i++)
    {
        cout << pow(num, i) << endl;
    }

    return 0;
}
