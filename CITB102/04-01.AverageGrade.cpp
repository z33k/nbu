#include <iostream>
#include <string>
using namespace std;

int main()
{
	string firstName, lastName, facultyNumber;

	cout << "Please enter your first and last name, followed by your faculty number: " << endl;
	cin >> firstName >> lastName >> facultyNumber;

	cout << "Please enter your grades separated with space: ";
	int enteredNum = 0;
	double averageScore, score = 0, counter = 0;

	for (int i = 0; i < 3 ; ++i)
	{
		cin >> enteredNum;
		if (enteredNum >= 2 && enteredNum <= 6)
		{
			score += enteredNum;
			counter++;
		}
	}

	averageScore = score / counter;
	cout << firstName +  " " + lastName + " - " << facultyNumber << ": " << averageScore << endl;

	return 0;
}
