#include <iostream>
#include <string>

using namespace std;

int main()
{
	int firstNum, secondNum;

	cout << "Please enter your first number: ";
	cin >> firstNum;
	
	cout << "Please enter your second number: ";
	cin >> secondNum;
	
	if (firstNum > secondNum)
	{
		cout << "The first number is greater!" << endl;
	}
	else
	{
		cout << "The second number is greater!" << endl;
	} 
	
	return 0;
}