#include <iostream>
using namespace std;

int main()
{
    int biggestNumber = 0;
    int firstNumber, secondNumber, thirdNumber;

    cout << "Enter the first number: ";
    cin >> firstNumber;

    cout << "Enter the second number: ";
    cin >> secondNumber;

    cout << "Enter the third number: ";
    cin >> thirdNumber;

    if (firstNumber > biggestNumber)
    {
        biggestNumber = firstNumber;
    }

    if (secondNumber > biggestNumber)
    {
        biggestNumber = secondNumber;
    }

    if (thirdNumber > biggestNumber)
    {
        biggestNumber = thirdNumber;
    }

    cout << "Biggest number is: " << biggestNumber << endl;

    return 0;
}
