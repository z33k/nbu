#include <iostream>
using namespace std;

int main()
{
	float a, b;

	cout << "Enter the first number: ";
	cin >> a;

	cout << "Enter the second number: ";
	cin >> b;
	
	if ( b > 0 )
	{
		cout << a / b;
	}
	else
	{
		cout << "Cannot divide by 0!";
	}
	
	return 0;
}