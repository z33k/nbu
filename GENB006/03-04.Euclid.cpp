#include <iostream>
using namespace std;

int main()
{
    int a, b, c;
    int i = 0;

    cout << "Enter first number: ";
    cin >> a;

    cout << "Enter second number: ";
    cin >> b;

    do
    {
        c = a % b;
        a = b;
        i++;
        cout << i << ". Line!" << endl;
    }
    while ( c > 0);
    cout << c;
}
/*
loopstart:
        if (i < 8 )
        {
            cout << i << ". Loop line!" << endl;
            i++;
            goto loopstart;
        }
        else
        {
            goto loopend;
        }
    loopend:
        return 0;
*/
