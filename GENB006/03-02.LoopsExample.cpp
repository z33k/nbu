#include <iostream>
using namespace std;

int main()
{
    int i = 1;
    while (i <= 7)
    {
        cout << i << ". I'm a while loop!" << endl;
        i++;
    }

    i = 1;
    do
    {
        cout << i << ". I'm a do-while loop!" << endl;
        i++;
    }
    while (i <= 7);

    i = 1;
    for (i = 1; i <= 7; i++)
    {
        cout << i << ". I'm a for loop!" << endl;
    }

    return 0;
}
