#include <iostream>
using namespace std;

int main()
{
    int i = 1;
    loopstart:
        if (i < 8 )
        {
            cout << i << ". Loop line!" << endl;
            i++;
            goto loopstart;
        }
        else
        {
            goto loopend;
        }
    loopend:
        return 0;
}
